#include <yuchentest/base/AsyncLogging.h>
#include <yuchentest/base/Timestamp.h>
#include <yuchentest/base/LogFile.h>

#include <vector>
#include <stdio.h>

using namespace yuchen;

AsyncLogging::AsyncLogging(const std::string& basename,
		size_t rollSize,
		int flushInterval)
	:flushInterval_(flushInterval),
	running_(false),
	basename_(basename),
	rollSize_(rollSize),
	thread_(startThread, (void*)this, "Logging"),
	latch_(1),
	mutex_(),
	cond_(mutex_),
	currentBuffer_(new Buffer),
	nextBuffer_(new Buffer),
	buffers_()
{
	currentBuffer_->bzero();
	nextBuffer_->bzero();
	buffers_.reserve(16);
}

void AsyncLogging::append(const char* logline, int len)
{
	yuchen::MutexLockGuard lock(mutex_);
	if(currentBuffer_->avail() > len)
	{
		currentBuffer_->append(logline, len);
	}
	else
	{
		buffers_.push_back(currentBuffer_);

		if(nextBuffer_)
		{
			currentBuffer_ = nextBuffer_;
			nextBuffer_ = NULL;
		}
		else
		{
			currentBuffer_ = new Buffer;
		}
		currentBuffer_->append(logline, len);
		cond_.notify();
	}
}
void* AsyncLogging::startThread(void* obj)
{
	AsyncLogging* asy = static_cast<AsyncLogging*>(obj);
	asy->threadFunc();
	return NULL;
}

void AsyncLogging::threadFunc()
{
	assert(running_ = true);
	latch_.countDown();
	LogFile output(basename_, rollSize_, false);
	BufferPtr newBuffer1(new Buffer);
	BufferPtr newBuffer2(new Buffer);
	newBuffer1->bzero();
	newBuffer2->bzero();
	BufferVector buffersToWrite;
	buffersToWrite.reserve(16);
	while(running_)
	{
		assert(newBuffer1 && newBuffer1->length() ==0);
		assert(newBuffer2 && newBuffer2->length() ==0);
		assert(buffersToWrite.empty());

		{
			yuchen::MutexLockGuard lock(mutex_);
			if(buffers_.empty())
			{
				cond_.waitForSenconds(flushInterval_);
			}
			buffers_.push_back(currentBuffer_);
			currentBuffer_ = newBuffer1;
			newBuffer1 = NULL;
			buffersToWrite.swap(buffers_);
			if(!nextBuffer_)
			{
				nextBuffer_ = newBuffer2;
				newBuffer2 = NULL;
			}
		}

		assert(! buffersToWrite.empty());
		if(buffersToWrite.size() > 25)
		{
			char buf[256];
			snprintf(buf, sizeof buf, "Dropped log messages larger buffers\n");
			fputs(buf, stderr);
			output.append(buf, static_cast<int>(strlen(buf)));
			buffersToWrite.erase(buffersToWrite.begin()+2, buffersToWrite.end());
		}
		for(size_t i = 0; i < buffersToWrite.size(); ++i)
		{
			output.append(buffersToWrite[i]->data(), buffersToWrite[i]->length());
			delete buffersToWrite[i];
		}

		if(buffersToWrite.size() > 2)
		{
			buffersToWrite.resize(2);
		}

		if(!newBuffer1)
		{
			assert(!buffersToWrite.empty());
			newBuffer1 = new Buffer;
			buffersToWrite.pop_back();
		}

		if(!newBuffer2)
		{
			assert(!buffersToWrite.empty());
			newBuffer2 = new Buffer;
			buffersToWrite.pop_back();
		}

		buffersToWrite.clear();
		output.flush();
	}
	output.flush();
}

	



