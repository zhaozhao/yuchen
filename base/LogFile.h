#ifndef YUCHEN_BASE_LOGFILE_H
#define YUCHEN_BASE_LOGFILE_H

#include <yuchentest/base/Mutex.h>
#include <string>
#include <time.h>

namespace yuchen
{
	class LogFile
	{
		public:
			LogFile(const std::string &basename,
					size_t rollSize,
					bool threadSafe = true,
					int flushInterval = 3);
			~LogFile();

			void append(const char* logline, int len);
			void flush();

		private:
			void append_unlocked(const char* logline, int len);

			static std::string getLogFileName(const std::string& basename, time_t* now);
			void rollFile();

			const std::string basename_;
			const size_t rollSize_;
			const int flushInterval_;

			int count_;

			MutexLock* mutex_;
			time_t startOfPeiod_;
			time_t lastRoll_;
			time_t lastFlush_;
			class File;
			File* file_;

			const static int kCheckTimeRoll_ = 1024;
			const static int kRollPerSeconds_ = 60*60*24;
	};
}
#endif
