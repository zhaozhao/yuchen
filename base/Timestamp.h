#ifndef YUCHEN_BASE_TIMESTAMP_H
#define YUCHEN_BASE_TIMESTAMP_H
#include <sys/time.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#undef __STDC_FORMAT_MACROS
#include <stdio.h>
#include <string>
namespace yuchen
{
	class Timestamp
	{
		public:
			Timestamp()
				:microSecondsSinceEpoch_(0)
			{
			}

			explicit Timestamp(int64_t microSecondsSinceEpoch)
				:microSecondsSinceEpoch_(microSecondsSinceEpoch)
			{
			}

			std::string toString()const
			{
				char buf[32] = {0};
				int64_t seconds = microSecondsSinceEpoch_/ kMicroSecondsPerSecond;
				int64_t microSeconds = microSecondsSinceEpoch_% kMicroSecondsPerSecond;
				snprintf(buf, sizeof(buf)-1,"%"PRId64".%06"PRId64"", seconds, microSeconds);
				return buf;
			}

			int64_t microSecondsSinceEpoch() const { return microSecondsSinceEpoch_; }
			time_t secondsSinceEpoch() const { return microSecondsSinceEpoch_/kMicroSecondsPerSecond; }
			static Timestamp now()
			{
				struct timeval tv;
				gettimeofday(&tv, NULL);
				int64_t seconds = tv.tv_sec;
				return Timestamp(seconds * kMicroSecondsPerSecond + tv.tv_usec);
			}

			static Timestamp invalid()
			{
				return Timestamp();
			}
			static const  int kMicroSecondsPerSecond = 1000 * 1000;
		private:
			int64_t microSecondsSinceEpoch_;
	};

	//const int Timestamp::kMicroSecondsPerSecond;
	inline double timeDifference(Timestamp high, Timestamp low)
	{
		int64_t diff = high.microSecondsSinceEpoch() - low.microSecondsSinceEpoch();
		return static_cast<double>(diff) / Timestamp::kMicroSecondsPerSecond;
	}

	inline Timestamp addTime(Timestamp timestamp, double seconds)
	{
		int64_t delta = static_cast<int64_t>(seconds * Timestamp::kMicroSecondsPerSecond);
		return Timestamp(timestamp.microSecondsSinceEpoch() + delta);
	}

}


#endif
