#ifndef YUCHEN_BASE_MUTEX_H
#define YUCHEN_BASE_MUTEX_H

#include <assert.h>
#include <pthread.h>

namespace yuchen
{
	class MutexLock
	{
		public:
			MutexLock()
			{
				int ret = pthread_mutex_init(&mutex_, NULL);
				assert(ret == 0);
			}
			~MutexLock()
			{
				int ret = pthread_mutex_destroy(&mutex_);
				assert(ret == 0);
			}
			void lock()
			{
				pthread_mutex_lock(&mutex_);
			}
			void unlock()
			{
				pthread_mutex_unlock(&mutex_);
			}
			pthread_mutex_t* getPthreadMutex()
			{
				return &mutex_;
			}
		private:
			pthread_mutex_t mutex_;
	};
	
	class MutexLockGuard
	{
		public:
			explicit MutexLockGuard(MutexLock& mutex)
				:mutex_(mutex)
			{
				mutex_.lock();
			}
			~MutexLockGuard()
			{
				mutex_.unlock();
			}
		private:
			MutexLock& mutex_;
	};
}

//Prevent misuselike:
//MutexLockGuard(mutex_);
//A tempory object dosesn't hold the lock for long!
#define MutexLockGuard(x) error "Missing guard object name"

#endif
