#include <yuchentest/base/LogFile.h>
#include <yuchentest/base/Logging.h>
#include <unistd.h>
yuchen::LogFile * g_logFile;

void outputFunc(const char* msg, int len)
{
	g_logFile->append(msg, len);
}

void flushFunc()
{
	g_logFile->flush();
}

int main(int argc, char* argv[])
{
	char name[256];
	strncpy(name, argv[0], 256);
	g_logFile = new yuchen::LogFile(::basename(name), 200*1000);
	yuchen::Logger::setOutput(outputFunc);
	yuchen::Logger::setFlush(flushFunc);

	std::string line = "1234567890 abcedfghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
	for(int i = 0; i < 10000; ++i)
	{
		LOG_INFO<<line<<i;
		usleep(1000);
	}
}
