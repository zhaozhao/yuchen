#include <yuchentest/base/BlockingQueue.h>
#include <yuchentest/base/CountDownLatch.h>
#include <yuchentest/base/ThreadPool.h>
#include <yuchentest/base/Thread.h>
#include <yuchentest/base/Logging.h>

#include <string>
#include <unistd.h>

using namespace yuchen;
BlockingQueue<std::string> g_blockingqueue;
CountDownLatch* g_countdownlatch;
void threadFunc()
{
	if(g_countdownlatch->getCount())
	{
		std::string d(g_blockingqueue.take());
		printf("tid=%d, get data=%s\n",CurrentThread::tid(), d.c_str());
		g_countdownlatch->countDown();
	}
}
int main()
{
	ThreadPool pool("pool");
	pool.start(5);
	LOG_INFO<<"pool created";
	printf("Main threadid=%d\n", CurrentThread::tid());

	int times=500;
	g_countdownlatch = new CountDownLatch(times);
	for(int i = 0; i < times; ++i)
	{
		char buf[32];
		snprintf(buf, sizeof buf, "hello %d", i);
		g_blockingqueue.put(buf);
	}
	LOG_INFO<<"queue push finished!";
	for(int i = 0; i < times; ++i)
	{
		pool.run(threadFunc);
	}
	LOG_INFO<<"stop push end";
	g_countdownlatch->wait();
	assert(g_blockingqueue.size() == 0);
	LOG_INFO<<"test end";

	delete g_countdownlatch;
}
