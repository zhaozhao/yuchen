#include <cstdio>
#include <unistd.h>
#include <sys/types.h>
#include <yuchentest/base/Thread.h>
void* threadFunc(void *)
{
	printf("tid=%d\n", yuchen::CurrentThread::tid());
	printf("thread name:%s\n", yuchen::CurrentThread::name());
	return NULL;
}


int main()
{
	printf("main thread::pid=%d\n", ::getpid());

	yuchen::Thread t1(threadFunc);
	printf("thread 1 started\n");
	t1.start();
	t1.join();
	printf("thread 1 joined\n");


	yuchen::Thread t2(threadFunc);
	printf("thread 2 started\n");
	t2.start();
	t2.join();
	printf("thread 2 joined\n");

	yuchen::Thread t3(threadFunc);
	printf("thread 3 started\n");
	t3.start();
	t3.join();
	printf("thread 3 joined\n");
	
	printf("number of created threads %d\n", yuchen::Thread::numCreated());
	printf("main thread name:%s\n", yuchen::CurrentThread::name());
}
