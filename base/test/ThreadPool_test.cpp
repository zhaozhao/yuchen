#include <yuchentest/base/ThreadPool.h>
#include <unistd.h>
#include <cstdio>

void print()
{
	printf("tid=%d\n", yuchen::CurrentThread::tid());
}

int main()
{	
	yuchen::ThreadPool pool("MainThreadPool");
	pool.start(5);
	printf("pool created\n");

	pool.run(print);
	pool.run(print);

	for(int i = 0; i < 100; ++i)
	{
		pool.run(print);
	}

	sleep(1);
	pool.stop();
	printf("pool stop\n");

	return 0;
}
