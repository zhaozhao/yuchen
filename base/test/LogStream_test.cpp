#include <yuchentest/base/LogStream.h>
#include <iostream>
#include <string>
using namespace yuchen;
int main()
{
	std::cout<<"test started!"<<std::endl;
	LogStream logstream;
	logstream<<"int 5: "<<5<<'\n';
	logstream<<"bool true: "<< true <<'\n';
	logstream<<"hex 32: "<< reinterpret_cast<const void*>(32) <<'\n';
	logstream<<"double 3.1415: "<<3.1415<<'\n';
	std::string str("hello");
	logstream<<"string \"hello\": "<< str <<'\n';
	logstream<<"fmt %d 10: "<< Fmt("%d",10) <<'\n';
	logstream<<"fmt %f 3.1415: "<< Fmt("%f", 3.1415) <<'\n';
	std::cout<<logstream.buffer().asString()<<std::endl;
	std::cout<<"test finished!"<<std::endl;
}

