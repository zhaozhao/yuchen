#include <yuchentest/base/ProcessInfo.h>
#include <cstdio>

int main()
{
	printf("pid = %d\n", yuchen::ProcessInfo::pid());
	printf("hostname = %s\n", yuchen::ProcessInfo::hostname().c_str());
	return 0;
}
