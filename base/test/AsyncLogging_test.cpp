#include <yuchentest/base/AsyncLogging.h>
#include <yuchentest/base/Logging.h>
#include <yuchentest/base/Timestamp.h>

#include <stdio.h>
#include <sys/resource.h>
#include <unistd.h>

int kRollSize = 500*1000*1000;

yuchen::AsyncLogging* g_asyncLog = NULL;

void asyncOutput(const char* msg, int len)
{
	g_asyncLog->append(msg, len);
}

void bench(bool longlog)
{
	yuchen::Logger::setOutput(asyncOutput);

	int cnt = 0;
	const int kBatch = 1000;
	std::string empty;
	std::string longStr(3000, 'X');
	longStr += " ";

	for(int t = 0; t < 30; ++t)
	{
		yuchen::Timestamp start = yuchen::Timestamp::now();
		for(int i = 0; i < kBatch; ++i)
		{
			LOG_INFO << "Hello 0123456789" << " abcdefghijklmnopqrstuvwxyz "
				<<(longlog? longStr:empty)
				<< cnt;
			++cnt;
		}
		yuchen::Timestamp end = yuchen::Timestamp::now();
		printf("%f\n", timeDifference(end, start)*1000000/kBatch);
		struct timespec ts = { 0, 500*1000*1000 };
		nanosleep(&ts, NULL);
	}
}

int main(int argc, char* argv[])
{
	{
		size_t kOneGB = 1000 * 1024 *1024;
		rlimit rl = {2*kOneGB, 2*kOneGB};
		setrlimit(RLIMIT_AS, &rl);
	}

	printf("pid = %d\n", getpid());

	char name[256];
	strncpy(name, argv[0], 256);
	yuchen::AsyncLogging log(::basename(name), kRollSize);
	log.start();
	g_asyncLog = &log;

	bool longlog = argc > 1;
	bench(longlog);
}
