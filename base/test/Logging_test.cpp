#include <yuchentest/base/Logging.h>
#include <yuchentest/base/ThreadPool.h>

#include <stdio.h>
#include <unistd.h>

void logInThread()
{
	LOG_INFO<<"logInThread";
	usleep(1000);
}

int main()
{
	yuchen::ThreadPool pool("pool");
	pool.start(5);
	pool.run(logInThread);
	pool.run(logInThread);
	pool.run(logInThread);
	pool.run(logInThread);
	pool.run(logInThread);


	LOG_TRACE<<"trace";
	LOG_DEBUG<<"debug";
	LOG_INFO<<"Hello";
	LOG_WARN<<"World";
	LOG_ERROR<<"Error";
	LOG_INFO<< sizeof(yuchen::Logger);
	LOG_INFO<< sizeof(yuchen::LogStream);
	LOG_INFO<< sizeof(yuchen::Fmt);
	LOG_INFO<< sizeof(yuchen::LogStream::Buffer);
}
