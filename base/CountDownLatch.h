#ifndef YUCHEN_BASE_COUNTDOWNLATCH_H
#define YUCHEN_BASE_COUNTDOWNLATCH_H

#include <yuchentest/base/Condition.h>
#include <yuchentest/base/Mutex.h>

namespace yuchen
{
	class CountDownLatch
	{
		public:
			explicit CountDownLatch(int count);

			void wait();

			void countDown();

			int getCount()const;

		private:
			mutable MutexLock mutex_;
			Condition condition_;
			int count_;
	};
}

#endif
