#ifndef YUCHEN_BASE_PROCESSINFO_H
#define YUCHEN_BASE_PROCESSINFO_H

#include <string>
#include <sys/types.h>

namespace yuchen
{
	namespace ProcessInfo
	{
		pid_t pid();
		std::string hostname();
	}
}

#endif
