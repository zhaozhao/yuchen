#include <yuchentest/base/ThreadPool.h>
#include <cstdio>
using namespace yuchen;

ThreadPool::ThreadPool(const std::string &name)
	:mutex_(),
	notEmpty_(mutex_),
	notfull_(mutex_),
	name_(name),
	maxQueueSize_(0),
	running_(false)
{
}

ThreadPool::~ThreadPool()
{
	if(running_)
	{
		stop();
	}
	//fix me: delete the pointers to Thread in threads_
}

void ThreadPool::start(int numThreads)
{
	assert(threads_.empty());
	running_ = true;
	threads_.reserve(numThreads);
	for(int i = 0; i < numThreads; ++i)
	{
		char id[32];
		snprintf(id, sizeof id, "%d",i);
		threads_.push_back(new yuchen::Thread(
				&startThreads, this, name_+id));
		threads_[i]->start();
	}
}

void ThreadPool::stop()
{
	{
		MutexLockGuard lock(mutex_);
		running_ = false;
		notEmpty_.notifyAll();
	}
	for(std::vector<Thread*>::iterator i = threads_.begin(); i != threads_.end(); ++i)
	{
		(*i)->join();
	}
	
}

void ThreadPool::run(const Task& task)
{
	if(threads_.empty())
	{
		task();
	}
	else
	{
		MutexLockGuard lock(mutex_);
		while(isFull())
		{
			notfull_.wait();
		}
		assert(!isFull());

		queue_.push_back(task);
		notEmpty_.notify();
	}
}


bool ThreadPool::isFull() const
{
	return maxQueueSize_ > 0 && queue_.size() >= maxQueueSize_;
}
void ThreadPool::runInThread()
{
	while(running_)
	{
		Task task(take());
		if(task)
		{
			task();
		}
	}
}
void* ThreadPool::startThreads(void* arg)
{
	ThreadPool* threadpool = static_cast<ThreadPool*>(arg);
	threadpool->runInThread();
	return NULL;
}
	

ThreadPool::Task ThreadPool::take()
{
	MutexLockGuard lock(mutex_);
	while(queue_.empty() && running_)
	{
		notEmpty_.wait();
	}
	Task task(NULL);
	if(!queue_.empty())
	{
		task = queue_.front();
		queue_.pop_front();
		if(maxQueueSize_ > 0)
		{
			notfull_.notify();
		}
	}
	return task;
}
