#include <yuchentest/base/Thread.h>
#include <yuchentest/base/Atomic.h>

#include <unistd.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <pthread.h>

namespace yuchen
{
	namespace CurrentThread
	{
		__thread const char* t_threadName = "unknown";
	}
}

namespace
{
	__thread pid_t t_cachedTid = 0;
	pid_t gettid()
	{
		return static_cast<pid_t>(::syscall(SYS_gettid));
	}

	class ThreadNameInitializer
	{
		public:
			ThreadNameInitializer()
			{
				yuchen::CurrentThread::t_threadName="main";
			}
	};

	ThreadNameInitializer init;
}
using namespace yuchen;
pid_t CurrentThread::tid()
{
	if(t_cachedTid == 0)
	{
		t_cachedTid = gettid();
	}
	return t_cachedTid;
}

const char* CurrentThread::name()
{
	return t_threadName;
}

bool CurrentThread::isMainThread()
{
	return tid() == ::getpid();
}

AtomicInt32 Thread::numCreated_;

	Thread::Thread(const ThreadFunc & func, void* arg, const std::string& n)
:started_(false),
	joined_(false),
	pthreadId_(0),
	tid_(0),
	func_(func),
	arg_(arg),
	name_(n)
{
	numCreated_.increment();
}

Thread::~Thread()
{
	if(started_ && !joined_)
	{
		pthread_detach(pthreadId_);
	}
}

void Thread::start()
{
	started_ = true;
	pthread_create(&pthreadId_, NULL,  &startThread, this);
}

void Thread::join()
{
	joined_ = true;
	pthread_join(pthreadId_, NULL);
}

void* Thread::startThread(void* obj)
{
	Thread* thread = static_cast<Thread*>(obj);
	thread->runInThread();
	return NULL;
}

void Thread::runInThread()
{
	tid_ = CurrentThread::tid();
	yuchen::CurrentThread::t_threadName = name_.c_str();
	func_(arg_);
	yuchen::CurrentThread::t_threadName = "finished";
}

