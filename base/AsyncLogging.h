#ifndef YUCHEN_BASE_ASYNCLOGGING_H
#define YUCHEN_BASE_ASYNCLOGGING_H

#include <yuchentest/base/Thread.h>
#include <yuchentest/base/CountDownLatch.h>
#include <yuchentest/base/Condition.h>
#include <yuchentest/base/LogStream.h>
#include <vector>

namespace yuchen
{
	class AsyncLogging
	{
		public:

			AsyncLogging(const std::string& basename,
					size_t rollSize,
					int flushInterval = 3);
			~AsyncLogging()
			{
				if(running_)
				{
					stop();
				}
			}

			void append(const char* logline, int len);

			void start()
			{
				running_ = true;
				thread_.start();
				latch_.wait();
			}

			void stop()
			{
				running_ = false;
				cond_.notify();
				thread_.join();
			}

		private:
			AsyncLogging(const AsyncLogging&);
			void operator=(const AsyncLogging&);

			void threadFunc();
			static void* startThread(void*);

			typedef yuchen::detail::FixedBuffer<yuchen::detail::kLargeBuffer> Buffer;
			typedef std::vector<Buffer*> BufferVector;
			typedef Buffer* BufferPtr;

			const int flushInterval_;
			bool running_;
			std::string basename_;
			size_t rollSize_;
			yuchen::Thread thread_;
			yuchen::CountDownLatch latch_;
			yuchen::MutexLock mutex_;
			yuchen::Condition cond_;
			BufferPtr currentBuffer_;
			BufferPtr nextBuffer_;
			BufferVector buffers_;
	};
}

#endif

