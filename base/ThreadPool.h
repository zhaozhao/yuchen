#ifndef YUCHEN_BASE_THREADPOOL_H
#define YUCHEN_BASE_THREADPOOL_H

#include <yuchentest/base/Condition.h>
#include <yuchentest/base/Mutex.h>
#include <yuchentest/base/Thread.h>

#include <deque>
#include <vector>
#include <string> 

namespace yuchen
{
	class ThreadPool
	{
		public:
			typedef void(*Task)();
			explicit ThreadPool(const std::string& name = std::string());
			~ThreadPool();

			void setMaxQueueSize(int maxSize) { maxQueueSize_ = maxSize; }
			void start(int numThreads);
			void stop();

			void run(const Task& f);
		private:
			bool isFull() const;
			static void* startThreads(void*);
			void runInThread();
			Task take();

			MutexLock mutex_;
			Condition notEmpty_;
			Condition notfull_;
			std::string name_;
			std::vector<Thread*> threads_;
			std::deque<Task> queue_;
			size_t maxQueueSize_;
			bool running_;
	};
}
#endif
