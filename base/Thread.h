#ifndef YUCHEN_BASE_THREAD_H
#define YUCHEN_BASE_THREAD_H
#include <string>
#include <pthread.h>
#include <yuchentest/base/Atomic.h>
namespace yuchen
{
	class Thread
	{
		public:
			typedef void* (*ThreadFunc)(void *);
			Thread(const ThreadFunc &, void* arg = NULL, const std::string& name = std::string());
			~Thread();
			void start();
			void join();
			bool started() const { return started_; }
			pid_t tid() const { return tid_; }
			const std::string& name() const { return name_; }

			static int numCreated() { return numCreated_.get(); }

		private:
			static void* startThread(void* thread);
			void runInThread();

			bool started_;
			bool joined_;
			pthread_t pthreadId_;
			pid_t tid_;
			ThreadFunc func_;
			void* arg_;
			std::string name_;

			static AtomicInt32 numCreated_;
	};
	namespace CurrentThread
	{
		pid_t tid();
		const char* name();
		bool isMainThread();
	}

}
#endif
