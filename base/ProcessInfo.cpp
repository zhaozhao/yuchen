#include <yuchentest/base/ProcessInfo.h>
#include <unistd.h>


namespace yuchen
{
	namespace ProcessInfo
	{
		pid_t pid()
		{
			return ::getpid();
		}

		std::string hostname()
		{
			char buf[64] = "unknowhost";
			buf[sizeof(buf)-1] = '\0';
			::gethostname(buf, sizeof buf);
			return buf;
		}
	}
}
