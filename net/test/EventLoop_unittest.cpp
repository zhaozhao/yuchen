#include <yuchentest/net/EventLoop.h>
#include <yuchentest/base/Thread.h>
#include <cstdio>
#include <unistd.h>
#include <sys/types.h>

using namespace yuchen;
using namespace yuchen::net;
EventLoop* g_eventloop;

void *threadFunc(void *)
{
	printf("threadFunc():pid = %d, tid =%d\n", getpid(), CurrentThread::tid());
	assert(EventLoop::getEventLoopOfCurrentThread() == NULL);
	EventLoop loop;
	assert(EventLoop::getEventLoopOfCurrentThread() == &loop);
	
	loop.loop();
	printf("threadFunc():end\n");
	return 0;
}
void *g_threadFunc(void *)
{
	g_eventloop->loop();
	return 0;
}

int main()
{
	printf("main():pid = %d, tid =%d\n", getpid(), CurrentThread::tid());

	assert(EventLoop::getEventLoopOfCurrentThread() == NULL);
	EventLoop loop;
	g_eventloop = &loop;
	assert(EventLoop::getEventLoopOfCurrentThread() == &loop);

	Thread thread(threadFunc);
	thread.start();
	Thread g_thread(g_threadFunc);
	g_thread.start();

	printf("main():end\n");

	pthread_exit(NULL);
}
