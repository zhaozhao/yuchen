#ifndef YUCHEN_NET_TIMERID_H
#define YUCHEN_NET_TIMERID_H

namespace yuchen
{
	namespace net
	{
		class Timer;

		class TimerId
		{
			public:

				TimerId(Timer* timer)
					: value_(timer)
				{
				}

			private:

				Timer* value_;
		};
	}
}

#endif 
