#include <yuchentest/net/TimerQueue.h>
#include <yuchentest/base/Logging.h>
#include <sys/timerfd.h>

namespace yuchen
{
	namespace net
	{
		namespace detail
		{
			int createTimerfd()
			{
				int timerfd = ::timerfd_create(CLOCK_MONOTONIC,
						TFD_NONBLOCK | TFD_CLOEXEC);
				if(timerfd < 0)
				{
					LOG_SYSFATAL<<"Failed in timerfd_create";
				}
				return timerfd;
			}

			struct timespec howMuchTimerFromNow(Timestamp when)
			{
				int64_t microSeconds = when.microSecondsSinceEpoch()
					-Timestamp::now().microSecondsSinceEpoch();
				if(microSeconds < 100)
				{
					microSeconds = 100;
				}
				struct timespec ts;
				ts.tv_sec = static_cast<time_t>(
						microSeconds / Timestamp::kMicroSecondsPerSecond);
				ts.tv_nsec = static_cast<long>(
						(microSeconds % Timestamp::kMicroSecondsPerSecond) * 1000);
				return ts;
			}

			void readTimerfd(int timerfd, Timestamp now)
			{
				uint64_t howmany;
				ssize_t n = ::read(timerfd, &howmany, sizeof howmany);
				if(n != sizeof howmany)
				{
					LOG_ERROR << "TimerQueue::handleRead() reads "<<n<<" bytes instead of 8";
				}
			}

			void restTimerfd(int timerfd, Timestamp expiration)
			{
				struct itimerspec newValue;
				struct itimerspec oldValue;
				bzero(&newValue, sizeof newValue);
				bzero(&oldValue, sizeof oldValue);

				newValue.it_value = howMuchTimerFromNow(expiration);
				int ret = ::timerfd_settime(timerfd, 0, &newValue, &oldValue);
				if(ret)
				{
					LOG_SYSERR<< "timerfd_settime()";
				}
			}
		}
	}
}


using namespace yuchen;
using namespace yuchen::net;
using namespace yuchen::net::detail;

TimerQueue:: TimerQueue(EventLoop* loop)
	:loop_(loop),
	timerfd_(createTimerfd()),
	timerfdChannel_(loop, timerfd_),
	timers_()
{
	tim
