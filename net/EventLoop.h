#ifndef YUCHEN_NET_EVENTLOOP_H
#define YUCHEN_NET_EVENTLOOP_H
#include <yuchentest/base/Thread.h>
#include <assert.h>
#include <vector>
#include <memory>
namespace yuchen
{
	namespace net
	{
		class Poller;
		class Channel;

		class EventLoop
		{
			public:
				EventLoop();
				~EventLoop();

				void loop();

				void quit();

				void updateChannel(Channel* channel);

				void assertInLoopThread()
				{
					if(!isInLoopThread())
					{
						abortNotInLoopThread();
					}
				}

				bool isInLoopThread() const { return threadId_ == CurrentThread::tid(); }

				static EventLoop* getEventLoopOfCurrentThread();
			private:
				void abortNotInLoopThread();
				typedef std::vector<Channel*> ChannelList;

				bool looping_;
				bool quit_;
				const pid_t threadId_;
				std::auto_ptr<Poller> poller_;
				ChannelList activeChannels_;
		};
	}
}
#endif

