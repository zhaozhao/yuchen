set(net_SRCS
	EventLoop.cpp
	Channel.cpp
	Poller.cpp
	Timer.cpp
	)

add_library(yuchentest_net ${net_SRCS})
target_link_libraries(yuchentest_net yuchentest_base)

add_subdirectory(test)
