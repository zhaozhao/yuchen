#ifndef YUCHEN_NET_TIMER_H
#define YUCHEN_NET_TIMER_H

#include <yuchentest/base/Timestamp.h>
#include <yuchentest/net/Callbacks.h>
#include <yuchentest/base/Atomic.h>

namespace yuchen
{
	namespace net
	{
		class Timer
		{
			public:
				Timer(const TimerCallback& cb, Timestamp when, double interval)
					:callback_(cb),
					expiration_(when),
					interval_(interval),
					repeat_(interval > 0.0)
			{
			}
				void run() const
				{
					callback_();
				}

				Timestamp expiration() const { return expiration_; }
				bool repeat() const { return repeat_; }
				//int64_t sequence() const { return sequence_; }
				
				void restart(Timestamp now);

				//static int64_t numCreated() { return s_numCreated_.get(); }
				
			private:
				const TimerCallback callback_;
				Timestamp expiration_;
				const double interval_;
				const bool repeat_;
		};
	}
}



#endif
