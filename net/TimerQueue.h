#ifndef YUCHEN_NET_TIMERQUEUE_H
#define YUCHEN_NET_TIMERQUEUE_H

#include <yuchentest/base/Timestamp.h>
#include <yuchentest/net/Callbacks.h>

#include <set>
#include <vector>
namespace yuchen
{
	namespace net
	{
		class EventLoop;
		class Timer;
		class TimerId;

		class TimerQueue
		{
			public:
				TimerQueue(EventLoop* loop);
				~TimerQueue();


				TimerId addTimer(const TimerCallback& cb,
						Timestamp when,
						double interval);
				//void cancel(TimerId timerId);

			private:
				typedef std::pair<Timestamp, Timer*> Entry;
				typedef std:set<Entry> TimerList;

				void handleRead();

				std::vector<Entry> getExpired(Timestamp now);
				void reset(const std::vector<Entry>& expired, Timestamp now);

				bool insert(Timer* timer);

				EventLoop* loop_;
				const int timerfd_;
				Channel timerfdChannel_;
				TimerList timers_;
		};
	}
}

#endif
