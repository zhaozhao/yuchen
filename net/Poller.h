#ifndef YUCHEN_NET_POLLER_H
#define YUCHEN_NET_POLLER_H

#include <vector>
#include <map>
#include <yuchentest/net/EventLoop.h>
#include <yuchentest/base/Timestamp.h>
struct pollfd;
namespace yuchen
{
	namespace net
	{
		class Channel;
		
		class Poller
		{
			public:
				typedef std::vector<Channel*> ChannelList;
				Poller(EventLoop* loop);
				~Poller();

				Timestamp poll(int timeoutMs, ChannelList* activeChannels);

				void updateChannel(Channel* channel);

				void assertInLoopThread() { ownerLoop_->assertInLoopThread(); }

			private:
				void fillActiveChannels(int numEvents, ChannelList* activeChannels) const;

				typedef std::vector<struct pollfd> PollFdList;
				typedef std::map<int, Channel*> ChannelMap;

				EventLoop* ownerLoop_;
				PollFdList pollfds_;
				ChannelMap channels_;
		};
	}
}
#endif
