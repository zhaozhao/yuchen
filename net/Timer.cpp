#include <yuchentest/net/Timer.h>
using namespace yuchen;
using namespace yuchen::net;

void Timer::restart(Timestamp now)
{
	if(repeat_)
	{
		expiration_ = addTime(now, interval_);
	}
	else
	{
		expiration_ = Timestamp::invalid();
	}
}

